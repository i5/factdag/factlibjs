import {ISubscription} from "../datamodels/interfaces/ISubscription";
import {IUsesFromAuthority} from "../datamodels/interfaces/IUsesFromAuthority";
import {IUsesFromAuthorityString} from "../datamodels/interfaces/IUsesFromAuthorityString";
import {FactClient} from "../factClient/FactClient";

/**
 * Subclasses of the abstract SubscriptionManager provide mechanisms to be notified of changes in resources.
 */
export abstract class SubscriptionManager {

    protected constructor(private service: FactClient<any>) {
    }

    /**
     * Subclasses must implement the subscribe function in a way that automatically subscribes to all resources
     * identified in the configuration and takes a callback function that is called when a resource changes.
     * @param callback A callback function that will be called when a resource changes.
     * @param uses An array with the dependencies that the manager should subscribe to (possibly from the config.json)
     */
    public abstract async startSubscription(uses: IUsesFromAuthorityString[],
                                            callback: (resource: string) => void): Promise<any>;

    /**
     * Determines for which facts a subscription is needed on a per-broker-basis.
     * @param uses An array with the dependencies that the manager should subscribe to (possibly from the config.json)
     * @return An array of subscriptions mapping message brokers to relevant resources handled by that broker.
     */
    protected async getSubscriptions(uses: IUsesFromAuthorityString[]): Promise<ISubscription[]> {
        const authorities = await this.getUsedFactsAuthorities(uses);
        const subscriptions: ISubscription[] = [];
        for (const element of authorities) {
            const broker = await this.service.getResponsibleMessageBroker(element.authority);
            let isMerged = false;
            subscriptions.forEach((subscription: ISubscription) => {
                if (broker.resourceID.resourceURI.toString() === subscription.broker.resourceID.resourceURI.toString()) {
                    subscription.resources.push(...element.facts.map((factURI: string) => {
                        const url = element.authority.resourceID.resourceURI;
                        url.pathname = factURI;
                        return url;
                    }));
                    isMerged = true;
                }
            });
            if (!isMerged) {
                subscriptions.push({
                    broker,
                    resources: element.facts.map((factURI: string) => {
                        const url = element.authority.resourceID.resourceURI;
                        url.pathname = factURI;
                        return url;
                    }),
                });
            }
        }
        return subscriptions;
    }

    /**
     * Reads the facts and their authorities that are relevant for the execution of the process from the configuration.
     * @return An array grouping authorities and the facts that are used from this authority.
     */
    private async getUsedFactsAuthorities(authorityURIs: IUsesFromAuthorityString[]): Promise<IUsesFromAuthority[]> {
        const authorityPromises: Array<Promise<IUsesFromAuthority>> = authorityURIs.map(async (element: IUsesFromAuthorityString) => {
            const auth = await this.service.getLastAuthorityRevision(this.service.getIDFactory().createAuthorityResourceID(element.authority));
            const authElement: IUsesFromAuthority = {authority: auth, facts: element.facts};
            return authElement;
        });
        return await Promise.all(authorityPromises);
    }
}
