import {Namespaces} from "@i5/factlib-utils";
import {MessageBroker} from "../factDagResources/messageBroker";
import {FactCandidate} from "./factCandidate";
import {EmptyTrunk} from "../../../factClient/EmptyTrunk";
import {IResourceID} from "../../interfaces/IResourceID";

/**
 * Representation of a AuthorityCandidate. <br />
 * See also: [[FactCandidate]] and [[Authority]]
 */
export class AuthorityCandidate<TTrunk = EmptyTrunk> extends FactCandidate<TTrunk>{

    constructor(data?: string, resourceID?: IResourceID, trunk?: TTrunk) {
        super(data, resourceID, trunk);
        this.addAuthorityProv();
    }

    /**
     * Writes the rdf:type prov:Organization locally to the current authorityCandidate.
     * This should be present for every authority.
     */
    public addAuthorityProv(): void {
        this.addProvenanceType(Namespaces.PROV_TYPE.ORGANIZATION);
    }

    /**
     * Writes the fact:hasResponsibleBroker relation locally to the current authorityCandidate.
     * @param broker The broker fact for the broker that is responsible for this authority.
     */
    public addResposibleBroker(broker: MessageBroker<TTrunk>): void {
        this.addFactRelation(Namespaces.FACT_RELATION.HAS_RESPONSIBLE_BROKER, broker.resourceID.factURI);
    }
}
