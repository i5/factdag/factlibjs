import {Namespaces} from "@i5/factlib-utils";
import {Fact} from "../factDagResources/fact";
import {Process} from "../factDagResources/process";
import {ResourceCandidate} from "./resourceCandidate";
import {EmptyTrunk} from "../../../factClient/EmptyTrunk";
import {IResourceID} from "../../interfaces/IResourceID";

/**
 * Representation of a FactDAG-Process execution (PROV activity).
 * Every time a Process is executed, a new revision of the associated activity should be created.
 * An activity can record the [[Fact]]s that were used in the execution and the associated [[Process]] in PROV format.
 */
export class ActivityCandidate<TTrunk = EmptyTrunk> extends ResourceCandidate<TTrunk> {

    constructor(data?: string, resourceID?: IResourceID, trunk?: TTrunk) {
        super(data, resourceID, trunk);
        this.addActivityProv();
    }

    /**
     * Writes the rdf:type prov:Activity locally to the current resourceCandidate.
     * This should be present for every activity.
     */
    public addActivityProv(): void {
        this.addProvenanceType(Namespaces.PROV_TYPE.ACTIVITY);
    }

    /**
     * Removes all prov:used triples from the activityCandidate in the local store.
     */
    public clearProvUse(): void {
        this.replaceProvenanceRelation(Namespaces.PROV_RELATION.USED);
    }

    /**
     * Adds a single prov:used triple to the current activityCandidate in the local store linking to the given fact
     * and remove all previous prov:used relations.
     * @param fact Single fact that should be referenced as prov:used in the current activityCandidate
     */
    public useFact(fact: Fact<TTrunk>): void {
        this.replaceProvenanceRelation(Namespaces.PROV_RELATION.USED, fact.factID.factURI);
    }

    /**
     * Adds a single prov:used triple to the current activityCandidate in the local store linking to the given fact.
     * @param fact Single fact that should be referenced as prov:used in the current activityCandidate
     */
    public addUseFact(fact: Fact<TTrunk>): void {
        this.addProvenanceRelation(Namespaces.PROV_RELATION.USED, fact.factID.factURI);
    }

    /**
     * Replaces all existing prov:used triples from the current activityCandidate in the local store
     * with prov:used triples linking to the given Facts.
     * @param facts Array of Facts that should be referenced as prov:used in the current activityCandidate
     */
    public useFacts(facts: Fact<TTrunk> []): void {
        this.clearProvUse();
        for (const fact of facts) {
            this.addUseFact(fact);
        }
    }

    /**
     * Locally associates the activityCandidate with one process and deletes all other associations.
     * @param process The process object that this activityCandidate should be associated with.
     */
    public associateWithProcess(process: Process<TTrunk>): void {
        this.replaceProvenanceRelation(Namespaces.PROV_RELATION.WAS_ASSOCIATED_WITH, process.factID.factURI);
    }

    /**
     * Adds a rdfs:comment providing possible insight into the activities context.
     * @param comment The comment to be associated with the activityCandidate. 
     */
    public addComment(comment: string) {
        const rdf = require('rdflib');
        const rdfs = new rdf.Namespace("http://www.w3.org/2000/01/rdf-schema#");
        const res = this.store.sym(this.resourceID.resourceURI.toString());
        this.store.add(res, rdfs("comment"), comment, res.doc());
    }

    /**
     * Removes all rdfs:comment triples from the activityCandidate in the local store.
     */
    public clearComment() {
        const rdf = require('rdflib');
        const rdfs = new rdf.Namespace("http://www.w3.org/2000/01/rdf-schema#");
        const res = this.store.sym(this.resourceID.resourceURI.toString());
        this.store.removeMany(res, rdfs("comment"), null, null, false);
    }
}
