import {EmptyTrunk} from "../../../factClient/EmptyTrunk";
import {IResourceID} from "../../interfaces/IResourceID";
import {Stream} from "stream";
import {FactCandidate} from "./factCandidate";

/**
 * Representation of a BinaryFactCandidate. <br />
 * It is a FactCandidate with an additional binary payload.
 */
export class BinaryFactCandidate<TTrunk = EmptyTrunk> extends FactCandidate<TTrunk> {
    constructor(private _binaryContent: Stream | string, store: any, private _contentType: string | undefined, resourceID?: IResourceID, trunk?: TTrunk) {
        super(store, resourceID, trunk);
    }


    get binaryContent(): Stream | string {
        return this._binaryContent;
    }

    get contentType(): string | undefined {
        return this._contentType;
    }
}
