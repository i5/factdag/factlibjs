import {Namespaces} from "@i5/factlib-utils";
import {ProcessCandidate} from "../candidates/processCandidate";
import {Fact} from "./fact";
import {IFactID} from "../../interfaces/IFactID";
import {IResourceID} from "../../interfaces/IResourceID";

/**
 * Representation of a FactDAG-Process.
 */
export class Process<TTrunk = any> extends Fact<TTrunk> {

    constructor(store: any, factID: IFactID, authorityResourceID: IResourceID, trunk: TTrunk) {
        super(store, factID, authorityResourceID, trunk);
    }

    /**
     * Retrieves the URI of the authority that is responsible for this process from the local store.
     * @return The URI of the authority that is responsible for this fact.
     */
    public getResponsibileAuthorityURI(): string {
        const rdfResult = this.queryStore(Namespaces.PROV(Namespaces.PROV_RELATION.ACTED_ON_BEHALF_OF));
        return rdfResult[0].object.value;
    }

    /**
     * Retrieves the location of the repository that reflects the current state of that process from the local store.
     * @return A URL of a Git repository in its state after a commit.
     */
    public getCodeRepository(): URL {
        const rdfResult = this.queryStore(Namespaces.SC(Namespaces.SCHEMA_RELATION.CODE_REPOSITORY));
        return new URL(rdfResult[0].object.value);
    }

    /**
     * Generates a mutable [[ProcessCandidate]] containing the data from the current, immutable Process.
     */
    public createCandidate(): ProcessCandidate<TTrunk> {
        const candidate = new ProcessCandidate<TTrunk>(this.serialize(), this.factID, this.trunk);
        candidate.wasRevisionOf(this);
        candidate.attributeToAuthority(this.authorityResourceID);
        candidate.clearProv();
        return candidate;
    }
}
