import {Fact} from "./fact";
import {BinaryFactCandidate} from "../candidates/binaryFactCandidate"
import {Stream} from "stream";
import {IFactID} from "../../interfaces/IFactID";
import {IResourceID} from "../../interfaces/IResourceID";

/**
 * Representation of a BinaryFact. <br />
 * BinaryFacts are Facts with an additional binary payload.
 */
export class BinaryFact<TTrunk = any> extends Fact {
    constructor(private _binaryContent: Stream | string, store: any, factID: IFactID, authorityResourceID: IResourceID, trunk: TTrunk, private _contentType?: string) {
        super(store, factID, authorityResourceID, trunk);
    }


    get binaryContent(): Stream | string {
        return this._binaryContent;
    }

    get contentType(): string | undefined{
        return this._contentType;
    }

    /**
     * Generates a mutable [[BinaryFactCandidate]] containing the data from the current, immutable BinaryFact.
     */
     createBinaryCandidate() {
        const candidate = new BinaryFactCandidate(this._binaryContent, this.serialize(), this._contentType, this.resourceID, this.trunk);
        candidate.clearProv();
        candidate.addEntityProv();
        candidate.wasRevisionOf(this);
        candidate.attributeToAuthority(this.authorityResourceID);
        return candidate;
    }
}