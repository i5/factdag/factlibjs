import {logger, Namespaces} from "@i5/factlib-utils";
import {AuthorityCandidate} from "../candidates/authorityCandidate";
import {Fact} from "./fact";
import {IFactID} from "../../interfaces/IFactID";
import {IResourceID} from "../../interfaces/IResourceID";

/**
 * Representation of a FactDAG-Authority.
 * Authorities are responsible for the Facts that are associated with them.
 */
export class Authority<TTrunk = any> extends Fact<TTrunk> {
    constructor(store: any, factID: IFactID, authorityResourceID: IResourceID, trunk: TTrunk) {
        super(store, factID, authorityResourceID, trunk);
    }
    /**
     * Retrieves the URI of a resource describing the message broker that is responsible for notifying processes of
     * changes in resources under this authority.
     * @return A URI of a resource describing a message broker.
     * It doesn't return the URI of the broker itself.
     */
    public getMessageBrokerURI(): string {
        const rdfResult = this.queryStore(Namespaces.FACT(Namespaces.FACT_RELATION.HAS_RESPONSIBLE_BROKER));
        return rdfResult[0].object.value;
    }

    public isValid(): boolean {
        const rdfResult = this.queryStore(Namespaces.RDF(Namespaces.RDF_RELATION.TYPE),
            Namespaces.PROV(Namespaces.PROV_TYPE.ORGANIZATION));
        try {
            if (rdfResult[0].object.value === Namespaces.PROV(Namespaces.PROV_TYPE.ORGANIZATION).value) {
                return true;
            } else {
                logger.warn("Authority is not a prov:Organization!");
                return false;
            }
        } catch (e) {
            logger.warn("Authority is not a prov:Organization!");
            return false;
        }
    }

    /**
     * Generates a mutable [[AuthorityCandidate]] containing the data from the current, immutable Authority.
     */
    public createCandidate(): AuthorityCandidate<TTrunk> {
        const candidate = new AuthorityCandidate<TTrunk>(this.serialize(), this.resourceID, this.trunk);
        candidate.wasRevisionOf(this);
        candidate.clearProv();
        return candidate;
    }
}
