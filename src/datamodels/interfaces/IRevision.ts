import {IFactID} from "./IFactID";

export interface IRevision<T> {
    readonly factID: IFactID;
    readonly location: T;

}