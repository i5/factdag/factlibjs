import {Authority} from "../factdag/factDagResources/authority";

export interface IUsesFromAuthority {
    authority: Authority;
    facts: string[];
}