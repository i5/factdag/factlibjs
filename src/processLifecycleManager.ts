import {FactCandidate} from "./datamodels/factdag/candidates/factCandidate";
import {BinaryFactCandidate} from "./datamodels/factdag/candidates/binaryFactCandidate"
import {Activity} from "./datamodels/factdag/factDagResources/activity";
import {Authority} from "./datamodels/factdag/factDagResources/authority";
import {Directory} from "./datamodels/factdag/factDagResources/directory";
import {Fact} from "./datamodels/factdag/factDagResources/fact";
import {Process} from "./datamodels/factdag/factDagResources/process";
import {IUsesFromAuthorityString} from "./datamodels/interfaces/IUsesFromAuthorityString";
import {logger} from "@i5/factlib-utils";
import {StompClient} from "./subscriptionManager/stompjsClient";
import {SubscriptionManager} from "./subscriptionManager/subscriptionManager";
import {FactClient} from "./factClient/FactClient";
import {LdpMementoClient} from "./factClient/ldpMementoClient/ldpMementoClient";
import {FactDagResourceFactory} from "./datamodels/interfaces/FactDagResourceFactory";
import {IDFactory} from "./datamodels/interfaces/IDFactory";
import {IResourceID} from "./datamodels/interfaces/IResourceID";
import {ApplicationConfig} from "./datamodels/interfaces/config/ApplicationConfig";

/**
 * Collection of functionality useful for the implementation of FactDAG-processes. <br />
 * Can establish the environment of a process by retrieving or creating the resources associated with
 *  the authority, the process itself and the activity associated with executions of the process.
 * The intended environment needs to be declared in the config.js file.
 */
export class ProcessLifecycleManager {
    private _authority: Authority | undefined;
    private _process: Process | undefined;
    private _activity: Activity | undefined;
    private _service: FactClient<any>;
    private _config: ApplicationConfig;
    private _codeRepository: string | undefined;
    private _dependencies: Map<string, Fact>;
    private _subscriptionManager: SubscriptionManager;
    private _factory: FactDagResourceFactory;
    private _idFactory: IDFactory;

    constructor(configuration: ApplicationConfig, codeRepository?: string) {
        this._config = configuration;
        this._codeRepository = codeRepository;
        this._service = new LdpMementoClient();
        this._factory = this._service.getFactDagResourceFactory();
        this._idFactory = this._service.getIDFactory();
        this._dependencies = new Map ();
        this._subscriptionManager = new StompClient(this._service);
    }

    public async initAuthority(): Promise<Authority> {
        logger.debug("Initialize Authority");
        try {
            this._authority = await this._service.getLastAuthorityRevision(this._idFactory.createAuthorityResourceID(this._config.authority.uri));
            logger.debug("Authority retrieved");
            if (!this._authority.isValid()) {
                const candidate = this._authority.createCandidate();
                this._authority = await this._service.createAuthorityRevision(candidate);
                return this._authority;
            }
            return this._authority;
        } catch (e) {
           return e;
        }
    }

    public async initProcess(authority: Authority): Promise<Process> {
        logger.debug("Init Process");
        let resultProcess;
        const resourceID = this._idFactory.createResourceID(this._config.authority.uri, this._config.process.location);
        if (await this._service.isResourceExistent(resourceID)) {
            resultProcess = await this._service.getLastProcessRevision(resourceID);
        } else {
            const candidate = this._factory.createEmptyProcessCandidate(this._idFactory.createResourceID(authority.resourceID.authorityID.toString(), this._config.process.location));
            candidate.actedOnBehalfOf(authority);
            const updatedProcess = await this._service.createProcessResource(candidate);
            this._process = updatedProcess;
            return updatedProcess;
        }
        if (this._config.process.checkLatest) {
            const commitRepoFromLDP = resultProcess.getCodeRepository();
            if (this._codeRepository !== commitRepoFromLDP.toString()) {
                throw("This process instance is not the process instance expected by the LDP!");
            }
        }
        logger.debug("Process retrieved");
        this._process = resultProcess;
        return resultProcess;
    }

    public async initActivity(authority: Authority, process: Process): Promise<Activity> {
        logger.debug("Initialize Activity");
        const resourceID = this._idFactory.createResourceID(this._config.authority.uri, this._config.activity.location);
        if (await this._service.isResourceExistent(resourceID)) {
            const resultActivity = await this._service.getLastActivityRevision(resourceID);
            logger.debug("Activity retrieved");
            this._activity = resultActivity;
            return resultActivity;
        } else {
            const activityCandidate = await this._factory.createEmptyActivityCandidate(this._idFactory.createResourceID(authority.resourceID.authorityID, this._config.activity.location));
            activityCandidate.associateWithProcess(process);
            const updatedActivity = await this._service.createActivityResource(activityCandidate);
            this._activity = updatedActivity;
            return updatedActivity;
        }
    }

    /**
     * Initializes the process environment.
     * That includes retrieving authority, process-fact and activity-resource.
     */
    public async initializeEnvironment() {
        logger.verbose("initializeEnvironment()");
        const authority = await this.initAuthority();
        const process = await this.initProcess(authority);
        await this.initActivity(authority, process);
    }

    /**
     * Startup sequence for a process.
     * That includes retrieving authority, process-fact and activity-resource as well as all dependency facts.
     * If a callback function is passed, subscriptions to the dependency facts are established.
     * @param callback A function that will be called when a notification from a message broker was received.
     */
    public async start(callback?: (resource: string) => void) {
        await this.initializeEnvironment();
        await this.loadDependencyFacts();
        logger.info("factlib.js - ProcessLifecycleManager successfully started!");
        if (callback) {
            await this._subscriptionManager.startSubscription(this._config.process.uses, async (resource) => {
                await this.updateDependencyFact(this._idFactory.parseURIToResourceID(resource));
                callback(resource);
            });
            logger.info("factlib.js - Subscriptions active");
        }
    }

    /**
     * Loads all dependency facts as declared in the configuration into a local dependency store.
     */
    public async loadDependencyFacts(): Promise<Fact[]> {
        const factPromises: Array<Promise<Fact>> = [];
        this._config.process.uses.forEach((element: IUsesFromAuthorityString) => {
            for (const factPath of element.facts) {
                const resourceID = this._idFactory.createResourceID(element.authority, factPath);
                factPromises.push(this._service.getLastFactRevision(resourceID));
            }
        });
        const facts = await Promise.all(factPromises).catch((e) => {
            logger.error("Can't load dependencies! %s", e);
            throw e;
        });
        this._dependencies.clear();
        facts.forEach((fact: Fact) => {
            this._dependencies.set(fact.resourceID.resourceURI.toString(), fact);
        });
        return facts;
    }

    /**
     * Updates a resource in the local dependency store by retrieving its current revision from an LDP.
     * @param resourceID The resourceID of a resource that is stored as a dependency of this process.
     */
    public async updateDependencyFact(resourceID: IResourceID): Promise<Fact> {
        logger.debug("updateDependencyFact(%s)", resourceID.toString());
        const fact = await this._service.getLastFactRevision(resourceID);
        this._dependencies.set(fact.resourceID.resourceURI.toString(), fact);
        return fact;
    }

    /**
     * A process can call this function with the FactCandidates that are supposed to be transferred to an LDP.
     * A corresponding Activity-Resource is automatically created with provenance information and the Candidates are
     * sent to the LDP.
     * @param factCandidates An array of Candidates representing the facts resulting from the process execution.
     * @param location The container that should contain a new Fact.
     * @param comment Optional comment describing the activities context. 
     * Only set if a new resource should be created that does not yet exist.
     */
    public async execProcess(factCandidates: FactCandidate[], location?: Directory, comment?: string): Promise<Fact[]> {
        logger.verbose("execProcess()");
        if (!this._activity || !this._process) { throw new Error("Activity or Process not available"); }
        const activityCandidate = this._activity.createCandidate();
        activityCandidate.associateWithProcess(this._process);
        activityCandidate.clearProvUse();
        this._dependencies.forEach((fact: Fact) => {
            activityCandidate.addUseFact(fact);
        });
        activityCandidate.clearComment();
        if (comment) {
            activityCandidate.addComment(comment);
        }
        const activityResult = await this._service.createActivityRevision(activityCandidate);
        this._activity = activityResult;
        logger.info("Activity created: %s", activityResult.factID.toString());
        const facts = factCandidates.map(async (candidate: FactCandidate) => {
            candidate.addGeneratingActivity(activityResult);
            const isRevision = await this._service.isResourceExistent(candidate.resourceID)
            let result: Fact;
            if(candidate instanceof BinaryFactCandidate) {
                if (!isRevision) {
                    result = await this._service.createBinaryFactResource(candidate);
                    logger.info("new Binary Fact created: %s", result.factID.toString());
                }
                else {
                    result = await this._service.createBinaryFactRevision(candidate);
                    logger.info("BinaryFactRevision created: %s", result.factID.toString());
                }
                return result;
            }
            else {
                if (!isRevision) {
                    result = await this._service.createFactResource(candidate);
                    logger.info("new Fact created: %s", result.factID.toString());
                } else {
                    result = await this._service.createFactRevision(candidate);
                    logger.info("FactRevision created: %s", result.factID.toString());
                }
                return result;
            }
        });
        return await Promise.all(facts);
    }

    get process(): Process {
        if (this._process) {
            return this._process;
        } else {
            throw new Error("Process is not initialized!");
        }
    }

    get activity(): Activity {
        if (this._activity) {
            return this._activity;
        } else {
            throw new Error("Activity is not initialized!");
        }
    }

    get authority(): Authority {
        if (this._authority) {
            return this._authority;
        } else {
            throw new Error("Activity is not initialized!");
        }
    }

    get dependencies(): Map<string, Fact> {
        return this._dependencies;
    }

    get service(): FactClient<any> {
        return this._service;
    }
}
