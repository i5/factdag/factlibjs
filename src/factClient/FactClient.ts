import {ActivityCandidate} from "../datamodels/factdag/candidates/activityCandidate";
import {AuthorityCandidate} from "../datamodels/factdag/candidates/authorityCandidate";
import {FactCandidate} from "../datamodels/factdag/candidates/factCandidate";
import {ProcessCandidate} from "../datamodels/factdag/candidates/processCandidate";
import {Activity} from "../datamodels/factdag/factDagResources/activity";
import {Authority} from "../datamodels/factdag/factDagResources/authority";
import {Directory} from "../datamodels/factdag/factDagResources/directory";
import {Fact} from "../datamodels/factdag/factDagResources/fact";
import {MessageBroker} from "../datamodels/factdag/factDagResources/messageBroker";
import {Process} from "../datamodels/factdag/factDagResources/process";
import {FactDagResource} from "../datamodels/factdag/factDagResources/factDagResource";
import {EmptyTrunk} from "./EmptyTrunk";
import {MessageBrokerCandidate} from "../datamodels/factdag/candidates/messageBrokerCandidate";
import {IDFactory} from "../datamodels/interfaces/IDFactory";
import {FactDagResourceFactory} from "../datamodels/interfaces/FactDagResourceFactory";
import {IFactID} from "../datamodels/interfaces/IFactID";
import {IResourceID} from "../datamodels/interfaces/IResourceID";
import {BinaryFact} from "../datamodels/factdag/factDagResources/binaryFact";
import {BinaryFactCandidate} from "../datamodels/factdag/candidates/binaryFactCandidate";
import {FACTDAG_CLASS} from "../classDictonary";

/**
 * An interface describing the methods that a FactClient implementation must provide
 * that handles the the low-level interaction with a storage system.
 */
export interface FactClient<TTrunk> {

    // CREATE
    /**
     * Creates a new directory.
     * @param resourceID The resourceID representing the location of the new directory.
     * @returns A directory object for the newly created directory.
     */
    createDirectory(resourceID: IResourceID): Promise<Directory<TTrunk>>;

    /**
     * Creates a new Fact resource from a candidate.
     * @param candidate The FactCandidate object representing the new, non-persistent resource.
     * @returns A Fact object representing the persisted Fact.
     */
    createFactResource(candidate: FactCandidate<EmptyTrunk | TTrunk>): Promise<Fact<TTrunk>>;

    /**
     * Creates a new Fact for a binary resource from a candidate.
     * @param candidate The FactCandidate object representing the new, non-persistent resource.
     * @returns A Fact object representing the persisted Fact.
     */
    createBinaryFactResource(candidate: FactCandidate<EmptyTrunk | TTrunk>): Promise<Fact<TTrunk>>;

    /**
     * Creates a new Activity resource from a candidate.
     * @param candidate The ActivityCandidate object representing the new, non-persistent resource.
     * @returns An Activity object representing the persisted Activity.
     */
    createActivityResource(candidate: ActivityCandidate<EmptyTrunk | TTrunk>): Promise<Activity<TTrunk>>;

    /**
     * Creates a new Process resource from a candidate.
     * @param candidate The ProcessCandidate object representing the new, non-persistent resource.
     * @returns A Process object representing the persisted Process.
     */
    createProcessResource(candidate: ProcessCandidate<EmptyTrunk | TTrunk>): Promise<Process<TTrunk>>;

    /**
     * Creates a new MessageBroker resource from a candidate.
     * @param candidate The MessageBrokerCandidate object representing the new, non-persistent resource.
     * @returns A MessageBroker object representing the persisted MessageBroker.
     */
    createMessageBrokerResource(candidate: MessageBrokerCandidate<EmptyTrunk | TTrunk>): Promise<MessageBroker<TTrunk>>;

    /**
     * Creates a new Authority resource from a candidate.
     * @param candidate The Authority object representing the new, non-persistent resource.
     * @returns An Authority object representing the persisted Authority.
     */
    createAuthorityResource(candidate: AuthorityCandidate<EmptyTrunk | TTrunk>): Promise<Authority<TTrunk>>;

    // READ Last
    /**
     * Retrieves the most recent revision of an arbitrary FactDagResource  from a backend.
     * @param resourceID The ResourceID object that identifies the desired resource.
     * @returns A FactDagResource object representing a specific state of a resource.
     */
    getLastResourceRevision(resourceID: IResourceID): Promise<FactDagResource<TTrunk>>;

    /**
     * Retrieves the most recent revision of a resource from a backend.
     * @param resourceID The ResourceID object that identifies the desired resource.
     * @returns A Fact object representing a specific state of a resource.
     */
    getLastFactRevision(resourceID: IResourceID): Promise<Fact<TTrunk>>;

    /**
     * Retrieves the most recent revision of a binary resource from a backend.
     * @param resourceID The ResourceID object that identifies the desired resource.
     * @param contentType The expected content type of the requested binary as a Mime-Type string.
     * @returns A Fact object representing a specific state of a resource.
     */
    getLastBinaryFactRevision(resourceID: IResourceID, contentType: string): Promise<BinaryFact<TTrunk>>;


    /**
     * Retrieves the most recent revision of an activity resource from a backend.
     * The last revision of an activity represents the latest execution of an associated process.
     * If the resource identified by the resourceID does not exist, is not parsable or not an Activity,
     * an error is thrown.
     * @param resourceID The ResourceID object that identifies the desired resource.
     * @returns An Activity object representing a specific process execution.
     */
    getLastActivityRevision(resourceID: IResourceID): Promise<Activity<TTrunk>>;

    /**
     * Retrieves the most recent revision of an Authority resource from a backend.
     * If the resource identified by the resourceID does not exist, is not parsable or not an Authority,
     * an error is thrown.
     * @param resourceID The ResourceID object that identifies the desired resource.
     * @returns An Authority object representing the organization responsible for a data collection.
     */
    getLastAuthorityRevision(resourceID: IResourceID): Promise<Authority<TTrunk>>;

    /**
     * Retrieves the most recent revision of a process resource from a backend.
     * If the resource identified by the resourceID does not exist, is not parsable or not a Process,
     * an error is thrown.
     * @param resourceID The ResourceID object that identifies the desired resource.
     * @returns An Process object representing the latest revision of the associated resource.
     */
    getLastProcessRevision(resourceID: IResourceID): Promise<Process<TTrunk>>;

    /**
     * Retrieves the most recent revision of the MessegeBroker Resource indicated by the given Authority.
     * @param authority The Authority object for which the responsible MessageBroker is to be retrieved.
     * @returns A MessageBroker object representingthe latest revision of the MessageBroker
     * responsible for the given Authority.
     */
    getResponsibleMessageBroker(authority: Authority<TTrunk>): Promise<MessageBroker<TTrunk>>;

    // READ SPECIFIC.
    /**
     * Retrieves a specific revision of an arbitrary FactDagResource from a backend.
     * If the resource identified by the factID does not exist or is not parsable an error is thrown.
     * @param factID The FactID object that identifies a specific revision of the desired resource.
     * @returns An FactDagResource object representing the desired revision of the associated resource.
     */
    getSpecificResourceRevision(factID: IFactID): Promise<FactDagResource<TTrunk>>;

    /**
     * Retrieves a specific revision of a resource from a backend.
     * If the resource identified by the factID does not exist, is not parsable or not a Fact,
     * an error is thrown.
     * @param factID The FactID object that identifies a specific revision of the desired resource.
     * @returns An Fact object representing the desired revision of the associated resource.
     */
    getSpecificFact(factID: IFactID): Promise<Fact<TTrunk>>;

    /**
     * Retrieves a specific revision of a binary resource from a backend.
     * If the resource identified by the factID does not exist, is not parsable or not a Fact,
     * an error is thrown.
     * @param factID The FactID object that identifies a specific revision of the desired resource.
     * @returns An Fact object representing the desired revision of the associated resource.
     */
    getSpecificBinaryFact(factID: IFactID): Promise<BinaryFact<TTrunk>>;

    /**
     * Retrieves a specific revision of an Activity resource from a backend.
     * @param factID The FactID object that identifies a specific revision of the desired Activity resource.
     * @returns An Activity object representing the desired revision of the associated Activity resource.
     */
    getSpecificActivityRevision(factID: IFactID): Promise<Activity<TTrunk>>;

    /**
     * Retrieves a specific revision of an Authority resource from a backend.
     * If the resource identified by the factID does not exist, is not parsable or not an Authority,
     * an error is thrown.
     * @param factID The FactID object that identifies a specific revision of the desired Authority resource.
     * @returns An Authority object representing the desired revision of the associated Authority resource.
     */
    getSpecificAuthorityRevision(factID: IFactID): Promise<Authority<TTrunk>>;

    /**
     * Retrieves a specific revision of a Process resource from a backend.
     * If the resource identified by the factID does not exist, is not parsable or not a Process,
     * an error is thrown.
     * @param factID The FactID object that identifies a specific revision of the desired Process resource.
     * @returns A Process object representing the desired revision of the associated Process resource.
     */
    getSpecificProcessRevison(factID: IFactID): Promise<Process<TTrunk>>;

    // READ OTHER
    /**
     * Get all revisions for a certain resource. Careful. Potentially expensive operation.
     * @param resource the resource for which all revisions should be retrieved.
     * @param type The type of the resource that should be returned.
     * @returns An array of all resource revisions of the requested resource.
     */
    getAllRevisions<ResourceType extends FactDagResource>(resource: ResourceType, type: FACTDAG_CLASS): Promise<ResourceType[]>;

    // UPDATE
    /**
     * Creates the revision of an existing Activity resource from a candidate.
     * @param candidate The ActivityCandidate object representing the new, non-persistent revision.
     * @returns An Activity object representing the persisted Activity.
     */
    createActivityRevision(candidate: ActivityCandidate<EmptyTrunk | TTrunk>): Promise<Activity<TTrunk>>;

    /**
     * Creates the revision of an existing Authority resource from a candidate.
     * @param candidate The Authority object representing the new, non-persistent revision.
     * @returns An Authority object representing the persisted Authority.
     */
    createAuthorityRevision(candidate: AuthorityCandidate<EmptyTrunk | TTrunk>): Promise<Authority<TTrunk>>;

    /**
     * Creates a revision of an existing Fact resource from a candidate.
     * @param candidate The FactCandidate object representing the new, non-persistent revision.
     * @returns A Fact object representing the persisted Fact.
     */
    createFactRevision(candidate: FactCandidate<EmptyTrunk | TTrunk>): Promise<Fact<TTrunk>>;

    /**
     * Creates a revision of an existing BinaryFact resource from a candidate.
     * @param candidate The FactCandidate object representing the new, non-persistent revision.
     * @returns A Fact object representing the persisted Fact.
     */
    createBinaryFactRevision(candidate: BinaryFactCandidate<EmptyTrunk | TTrunk>): Promise<BinaryFact<TTrunk>>;

    /**
     * Creates a Process revision of an existing process resource from a candidate.
     * @param candidate The ProcessCandidate object representing the new, non-persistent revision.
     * @returns A Process object representing the persisted Process Revision.
     */
    createProcessRevision(candidate: ProcessCandidate<EmptyTrunk | TTrunk>): Promise<Process<TTrunk>>;

    /**
     * Creates a MessageBroker revision of an existing Broker resource from a candidate.
     * @param candidate The MessageBrokerCandidate object representing the new, non-persistent revision.
     * @returns A MessageBroker object representing the persisted Broker Revision.
     */
    createBrokerRevision(candidate: MessageBrokerCandidate<EmptyTrunk | TTrunk>): Promise<MessageBroker<TTrunk>>;

    // DELETE
    /**
     * Deletes the current representation of a resource from its backend.
     * @param resourceID The ResourceID object that identifies the resource which should be deleted.
     */
    deleteResource(resourceID: IResourceID): Promise<void>;

    // Other Methods
    /**
     * Returns a directory from a resourceID.
     * If the directory identified by the resourceID does not exist or is not a directory,
     * an error is thrown.
     * @param resourceID The ResourceID object that identifies a specific directory.
     * @returns A Directory object listing the resources and directories contained in the given directory.
     */
    getDirectory(resourceID: IResourceID): Promise<Directory<TTrunk>>;

    /**
     * Checks if a certain resource exists in a backend.
     * @param resourceID The ResourceID object that may or may not identify an existing resource.
     * @returns True if the requested resource exists, False otherwise.
     */
    isResourceExistent(resourceID: IResourceID): Promise<boolean>;

    /**
     * Checks if a certain resource identified by its ID is a binary resource.
     * @param resourceID The ResourceID object that identifies an existing resource, which is potentially binary.
     * @returns True if the requested resource is binary, False otherwise.
     */
    isResourceBinary(resourceID: IResourceID): Promise<boolean>;

    getIDFactory(): IDFactory;
    getFactDagResourceFactory(): FactDagResourceFactory;
}
