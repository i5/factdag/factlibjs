import {LdpResource} from "@i5/ldpclient";
import {LdpResourceID} from "../mementoHandler/resources/ldpResourceID";
import {IDFactory} from "../../../datamodels/interfaces/IDFactory";
import {IResourceID} from "../../../datamodels/interfaces/IResourceID";
import {IFactID} from "../../../datamodels/interfaces/IFactID";
import {LdpFactID} from "../mementoHandler/resources/ldpFactID";

export class LDPIDFactory implements IDFactory {

    public generateFactIDFromLdpResource(ldpResource: LdpResource, resourceID: IResourceID): IFactID {
        const mementoDatetime = ldpResource.headers['memento-datetime'];
        let revisionID: Date | undefined = undefined;
        if (mementoDatetime) {
            revisionID = new Date(Date.parse(mementoDatetime));
        }
        const factID = new LdpFactID(resourceID.authorityID, resourceID.resourceID, revisionID);
        return factID;
    }

    public parseURIToFactID(idString: string): IFactID {
       const id = this.parseURIToID(idString);
       if (id instanceof LdpFactID) {
           return id;
       }
       throw new Error("Given String can't be parsed to a FactID, the revisionID could not be determined: " + idString);
    }

    public parseURIToResourceID(idString: string): LdpResourceID {
        return this.parseURIToID(idString, true);
    }

    private parseURIToID(idString: string, onlyResource = false): LdpResourceID {
        const scheme = idString.substring(0, idString.indexOf(':'));
        const data = idString.substring(idString.indexOf(':') + 1, idString.length);
        let url;
        let revisionID: Date | undefined = undefined;
        if (scheme === "fact") {
            if (data.substring(0,1) !== ":") {
                revisionID = new Date(data.substring(0, data.indexOf('Z') + 1));
                url = new URL(data.substring(data.indexOf("Z") + 2, data.length));
            } else {
                url = new URL(data.substring(1, data.length));
            }
        } else {
            url = new URL(idString);
            const version = url.searchParams.get("version");
            if (version) {
                revisionID = new Date(version);
            }
        }
        const authID = url.origin;
        const resourceID = url.pathname;
        if (onlyResource || revisionID === undefined) {
            return new LdpResourceID(authID, resourceID);
        } else {
            return new LdpFactID(authID, resourceID, revisionID);
        }
    }

    createResourceID(authorityID: string, resourceID: string): IResourceID {
        return new LdpResourceID(authorityID, resourceID);
    }

    createFactID(authorityID: string, resourceID: string, revisionID: string): IFactID {
        return new LdpFactID(authorityID, resourceID, new Date(Date.parse(revisionID)));
    }

    createAuthorityResourceID(authorityID: string): IResourceID {
        return new LdpResourceID(authorityID, "/.well-known/factdag-authority");
    }
}