import {Fact} from "../../../datamodels/factdag/factDagResources/fact";
import {Activity} from "../../../datamodels/factdag/factDagResources/activity";
import {Authority} from "../../../datamodels/factdag/factDagResources/authority";
import {MessageBroker} from "../../../datamodels/factdag/factDagResources/messageBroker";
import {Process} from "../../../datamodels/factdag/factDagResources/process";
import {ILdpNonRdfSource, LdpResource} from "@i5/ldpclient";
import {Namespaces} from "@i5/factlib-utils";
import {LdpTrunk} from "../resources/ldpTrunk";
import {FactDagResource} from "../../../datamodels/factdag/factDagResources/factDagResource";
import {Directory} from "../../../datamodels/factdag/factDagResources/directory";
import {LdpContainer} from "@i5/ldpclient";
import {ResourceCandidate} from "../../../datamodels/factdag/candidates/resourceCandidate";
import {EmptyTrunk} from "../../EmptyTrunk";
import {ActivityCandidate} from "../../../datamodels/factdag/candidates/activityCandidate";
import {AuthorityCandidate} from "../../../datamodels/factdag/candidates/authorityCandidate";
import {ProcessCandidate} from "../../../datamodels/factdag/candidates/processCandidate";
import {FactCandidate} from "../../../datamodels/factdag/candidates/factCandidate";
import {MessageBrokerCandidate} from "../../../datamodels/factdag/candidates/messageBrokerCandidate";
import {LDPIDFactory} from "./LDPIDFactory";
import {FactDagResourceFactory} from "../../../datamodels/interfaces/FactDagResourceFactory";
import {IFactID} from "../../../datamodels/interfaces/IFactID";
import {IResourceID} from "../../../datamodels/interfaces/IResourceID";
import {BinaryFact} from "../../../datamodels/factdag/factDagResources/binaryFact";
import {BinaryFactCandidate} from "../../../datamodels/factdag/candidates/binaryFactCandidate";
import {Stream} from "stream";
import {candidateDictionary, classDictionary, FACTDAG_CLASS} from "../../../classDictonary";

const rdf = require("rdflib");

export class LDPFactDagResourceFactory implements FactDagResourceFactory {

    constructor(private idFactory: LDPIDFactory) {
    }


    public static isActivity(ldpResource: LdpResource): boolean {
        const query = ldpResource.queryStore(Namespaces.RDF(Namespaces.RDF_RELATION.TYPE), Namespaces.PROV(Namespaces.PROV_TYPE.ACTIVITY));
        return !(query[0] === undefined);
    }

    public static isAuthority(ldpResource: LdpResource): boolean {
        const query = ldpResource.queryStore(Namespaces.RDF(Namespaces.RDF_RELATION.TYPE), Namespaces.PROV(Namespaces.PROV_TYPE.ORGANIZATION));
        return !(query[0] === undefined);
    }

    public static isProcess(ldpResource: LdpResource): boolean {
        const query = ldpResource.queryStore(Namespaces.RDF(Namespaces.RDF_RELATION.TYPE), Namespaces.PROV(Namespaces.PROV_TYPE.AGENT));
        return !(query[0] === undefined);
    }

    public static isMessageBroker(ldpResource: LdpResource): boolean {
        const query = ldpResource.queryStore(Namespaces.RDF(Namespaces.RDF_RELATION.TYPE), Namespaces.FACT(Namespaces.FACT_TYPE.BROKER));
        return !(query[0] === undefined);
    }

    public static isFact(ldpResource: LdpResource): boolean {
        const query = ldpResource.queryStore(Namespaces.RDF(Namespaces.RDF_RELATION.TYPE), Namespaces.PROV(Namespaces.PROV_TYPE.ENTITY));
        return !(query[0] === undefined);
    }

    public static isBinaryFact(ldpResource: LdpResource): boolean {
        const headers = ldpResource.getLinkHeaders("type");
        let result = false;
        headers.forEach((link: URL) => {
            if (link.hash == "#NonRDFSource") {
                result = true;
            }
        });
        return result;
    }

    public static determineCorrectResource(ldpResource: LdpResource): FACTDAG_CLASS {
        if (ldpResource instanceof LdpContainer) {
            return FACTDAG_CLASS.DIRECTORY;
        } else if (LDPFactDagResourceFactory.isActivity(ldpResource)) {
            return FACTDAG_CLASS.ACTIVITY;
        } else if (LDPFactDagResourceFactory.isMessageBroker(ldpResource)) {
            return FACTDAG_CLASS.BROKER;
        } else if (LDPFactDagResourceFactory.isAuthority(ldpResource)) {
            return FACTDAG_CLASS.AUTHORITY;
        } else if (LDPFactDagResourceFactory.isProcess(ldpResource)) {
            return FACTDAG_CLASS.PROCESS;
        } else if (LDPFactDagResourceFactory.isBinaryFact(ldpResource)) {
            return FACTDAG_CLASS.BINARY;
        } else if (LDPFactDagResourceFactory.isFact(ldpResource)) {
            return FACTDAG_CLASS.FACT;
        } else {
            throw new Error("This resources cannot be interpreted as a FactDAG element");
        }
    }

    public static generateTrunk(ldpResource: LdpResource, setEtag: boolean =true): LdpTrunk {
        const timemapHeader = ldpResource.getLinkHeaders("timemap");
        let timegateHeader = ldpResource.getLinkHeaders("timegate");
        if (!timegateHeader) {
            timegateHeader = ldpResource.getLinkHeaders("original timegate");
        }
        let timeGateURL: URL | undefined;
        let timemapURL: URL | undefined;
        if (timegateHeader) {
            timeGateURL = timegateHeader[0];
        }
        if (timemapURL) {
            timemapURL = timemapHeader[0];
        }
        let etag = '';
        if(setEtag) {
         etag = ldpResource.etag;   
        }
        return new LdpTrunk(etag, timemapURL, timeGateURL);
    }

    private createSpecificFactDagResource<FactType extends FactDagResource<LdpTrunk>>(ldpResource: LdpResource, resourceID: IResourceID, c: new(store: any, factID: IFactID, authorityResourceID: IResourceID, trunk: LdpTrunk) => FactType): FactType {
        const resource = new c(ldpResource.store,
            this.idFactory.generateFactIDFromLdpResource(ldpResource, resourceID),
            this.idFactory.createAuthorityResourceID(resourceID.authorityID),
            LDPFactDagResourceFactory.generateTrunk(ldpResource));
        return resource;
    }

    public createFactDagResourceFromType<FactType extends FactDagResource<LdpTrunk>>(ldpResource: LdpResource, resourceID: IResourceID, factClass: FACTDAG_CLASS, ignoreCurrentType = false): FactType {
        const resourceType = LDPFactDagResourceFactory.determineCorrectResource(ldpResource);
        if (resourceType === factClass || ignoreCurrentType) {
            return this.createSpecificFactDagResource<FactType>(ldpResource, resourceID, classDictionary[factClass]);
        } else {
            throw new Error("LDPResource has the wrong type! " + resourceType + " instead of " + factClass);
        }
    }

    public createFact(ldpResource: LdpResource, resourceID: IResourceID, ignoreCurrentType?: boolean): Fact<LdpTrunk> {
        return this.createFactDagResourceFromType(ldpResource, resourceID, FACTDAG_CLASS.FACT, ignoreCurrentType);
    }

    public createBinaryFact(ldpResource: ILdpNonRdfSource & LdpResource, description: Fact, resourceID: IResourceID, ignoreCurrentType?: boolean): BinaryFact<LdpTrunk> {
        if (LDPFactDagResourceFactory.isBinaryFact(ldpResource) || ignoreCurrentType) {
            const contentType = ldpResource.headers['content-type'];
            const resource = new BinaryFact(ldpResource.binaryContent, description.store,
                this.idFactory.generateFactIDFromLdpResource(ldpResource, resourceID),
                this.idFactory.createAuthorityResourceID(resourceID.authorityID),
                LDPFactDagResourceFactory.generateTrunk(ldpResource, false), contentType);
            return resource;
        } else {
            throw new Error("Not a binary");
        }
    }

    public createActivity(ldpResource: LdpResource, resourceID: IResourceID, ignoreCurrentType?: boolean): Activity<LdpTrunk> {
        return this.createFactDagResourceFromType(ldpResource, resourceID, FACTDAG_CLASS.ACTIVITY, ignoreCurrentType);
    }

    public createAuthority(ldpResource: LdpResource, resourceID: IResourceID, ignoreCurrentType?: boolean): Authority<LdpTrunk> {
        return this.createFactDagResourceFromType(ldpResource, resourceID, FACTDAG_CLASS.AUTHORITY, ignoreCurrentType);
    }

    public createDirectory(ldpResource: LdpContainer, resourceID: IResourceID): Directory<LdpTrunk> {
        if (ldpResource instanceof LdpContainer) {
            const trunk = LDPFactDagResourceFactory.generateTrunk(ldpResource);
            const list = ldpResource.getContainedResourceList();
            const containedDirectories: IResourceID[] = [];
            const containedResources: IResourceID[] = [];
            list.forEach((x) => {
                resourceID = this.idFactory.parseURIToResourceID(x);
                if (x.substring(x.length-1) == '/') {
                    containedDirectories.push(resourceID);
                } else {
                    containedResources.push(resourceID);
                }
            });
            return new Directory(resourceID, containedResources, containedDirectories, trunk);
        } else {
            throw new Error("LDPResource is not a Container!");
        }
    }

    public createMessageBroker(ldpResource: LdpResource, resourceID: IResourceID, ignoreCurrentType?: boolean): MessageBroker<LdpTrunk> {
        return this.createFactDagResourceFromType(ldpResource, resourceID, FACTDAG_CLASS.BROKER, ignoreCurrentType);
    }

    public createProcess(ldpResource: LdpResource, resourceID: IResourceID, ignoreCurrentType?: boolean): Process<LdpTrunk> {
        return this.createFactDagResourceFromType(ldpResource, resourceID, FACTDAG_CLASS.PROCESS, ignoreCurrentType);
    }

    public createFactDagResource(ldpResource: LdpResource, resourceID: IResourceID): FactDagResource<LdpTrunk> {
        const resourceClass = LDPFactDagResourceFactory.determineCorrectResource(ldpResource);
        return this.createSpecificFactDagResource(ldpResource, resourceID, classDictionary[resourceClass]);
    }

    public createEmptyAuthorityCandidate(resourceID: IResourceID): AuthorityCandidate<EmptyTrunk> {
        return this.createSpecificEmptyResourceCandidate(resourceID, FACTDAG_CLASS.AUTHORITY);
    }

    public createEmptyActivityCandidate(resourceID: IResourceID): ActivityCandidate<EmptyTrunk> {
        return this.createSpecificEmptyResourceCandidate(resourceID, FACTDAG_CLASS.ACTIVITY);
    }

    public createEmptyProcessCandidate(resourceID: IResourceID): ProcessCandidate<EmptyTrunk> {
        return this.createSpecificEmptyResourceCandidate(resourceID, FACTDAG_CLASS.PROCESS);
    }

    public createEmptyFactCandidate(resourceID: IResourceID): FactCandidate<EmptyTrunk> {
        return this.createSpecificEmptyResourceCandidate(resourceID, FACTDAG_CLASS.FACT);
    }

    public createEmptyBinaryFactCandidate(resourceID: IResourceID, binaryStream: Stream, contentType: string): BinaryFactCandidate<EmptyTrunk> {
        const store = rdf.graph();
        return new BinaryFactCandidate<EmptyTrunk>(binaryStream, store, contentType, resourceID);
    }

    public createEmptyBrokerCandidate(resourceID: IResourceID): MessageBrokerCandidate<EmptyTrunk> {
        return this.createSpecificEmptyResourceCandidate(resourceID, FACTDAG_CLASS.BROKER);
    }

    private createSpecificEmptyResourceCandidate<CandidateType extends ResourceCandidate<EmptyTrunk>>(resourceID: IResourceID, CandidateClass: FACTDAG_CLASS): CandidateType {
        const store =  rdf.graph();
        return new candidateDictionary[CandidateClass](store, resourceID);
    }
}