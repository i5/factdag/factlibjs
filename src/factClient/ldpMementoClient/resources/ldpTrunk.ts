
export class LdpTrunk  {

    constructor (private etag: string, private _timeMapURI: URL | undefined, private _timeGateURI: URL | undefined) {
    }


    get timeMapURI(): URL | undefined {
        return this._timeMapURI;
    }

    get timeGateURI(): URL | undefined {
        return this._timeGateURI;
    }

    get strongEtag(): string {
        if (this.etag && this.etag[0] === "W") {
            return this.etag.slice(2, this.etag.length);
        } else {
            return this.etag;
        }
    }
}