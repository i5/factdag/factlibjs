import {MementoHandler} from "./mementoHandler/mementoHandler";
import {Fact} from "../../datamodels/factdag/factDagResources/fact";
import {AxiosLDPClient} from "@i5/ldpclient";
import {AxiosLDPResourceFactory} from "@i5/ldpclient";
import {Authority} from "../../datamodels/factdag/factDagResources/authority";
import {LdpTrunk} from "./resources/ldpTrunk";
import {LDPFactDagResourceFactory} from "./factory/LDPFactDagResourceFactory";
import {FactDagResource} from "../../datamodels/factdag/factDagResources/factDagResource";
import {Directory} from "../../datamodels/factdag/factDagResources/directory";
import {ActivityCandidate} from "../../datamodels/factdag/candidates/activityCandidate";
import {Activity} from "../../datamodels/factdag/factDagResources/activity";
import {AuthorityCandidate} from "../../datamodels/factdag/candidates/authorityCandidate";
import {FactCandidate} from "../../datamodels/factdag/candidates/factCandidate";
import {Process} from "../../datamodels/factdag/factDagResources/process";
import {ProcessCandidate} from "../../datamodels/factdag/candidates/processCandidate";
import {ResourceCandidate} from "../../datamodels/factdag/candidates/resourceCandidate";
import {EmptyTrunk} from "../EmptyTrunk";
import {FactClient} from "../FactClient";
import {MessageBroker} from "../../datamodels/factdag/factDagResources/messageBroker";
import {MessageBrokerCandidate} from "../../datamodels/factdag/candidates/messageBrokerCandidate";
import {LDPIDFactory} from "./factory/LDPIDFactory";
import {IDFactory} from "../../datamodels/interfaces/IDFactory";
import {IResourceID} from "../../datamodels/interfaces/IResourceID";
import {IFactID} from "../../datamodels/interfaces/IFactID";
import {BinaryFactCandidate} from "../../datamodels/factdag/candidates/binaryFactCandidate";
import {BinaryFact} from "../../datamodels/factdag/factDagResources/binaryFact";
import {LdpNonRdfSource} from "@i5/ldpclient";
import {LdpResource} from "@i5/ldpclient";
import {IRevision} from "../../datamodels/interfaces/IRevision";
import {MementoLocation} from "./mementoHandler/resources/MementoLocation";
import {FACTDAG_CLASS} from "../../classDictonary";

export class LdpMementoClient implements FactClient<LdpTrunk> {
    private mementoHandler: MementoHandler;
    private ldpClient: AxiosLDPClient<LdpResource>;
    private ldpResourceFactory: AxiosLDPResourceFactory;
    private factDagResourceFactory: LDPFactDagResourceFactory;
    private idFactory: LDPIDFactory;

    constructor() {
        this.ldpResourceFactory = new AxiosLDPResourceFactory();
        this.ldpClient = new AxiosLDPClient<LdpResource>(this.ldpResourceFactory);
        this.mementoHandler = new MementoHandler(this.ldpClient);
        this.idFactory = new LDPIDFactory();
        this.factDagResourceFactory = new LDPFactDagResourceFactory(this.idFactory);
    }

    //CREATE
    public async createDirectory(resourceID: IResourceID): Promise<Directory<LdpTrunk>> {
        const ldpResource = await this.ldpClient.createLDPContainer(resourceID.directory, resourceID.resourceName);
        const givenResourceID = this.idFactory.parseURIToResourceID(ldpResource.resourceURI.toString());
        return this.factDagResourceFactory.createDirectory(ldpResource, givenResourceID);
    }

    public async createFactResource(candidate: FactCandidate): Promise<Fact<LdpTrunk>> {
        return this.createResource(candidate, FACTDAG_CLASS.FACT);
    }

    public async createBinaryFactResource(candidate: BinaryFactCandidate<EmptyTrunk | LdpTrunk>): Promise<BinaryFact<LdpTrunk>> {
        const location = candidate.resourceID.directory;
        const ldpResource = await this.ldpClient.createLDPNonRDFSource(location, candidate.resourceID.resourceName, candidate.binaryContent, candidate.contentType);

        // TODO
        /* This is a hacky solution to also generate the Metadata assocated with the binary that only works with Trellis.
           Instead the DescribedBy-Header should be used to determine the Description-Resource.
           However, Trellis creates URIs for Descriptions which are not compatible with the FactID format, which makes it difficult to handle them as their own resource.
        */
        const descriptionResource = await this.createResourceRevision(candidate, FACTDAG_CLASS.FACT);
        const givenResourceID = this.idFactory.parseURIToResourceID(ldpResource.resourceURI.toString());
        const binaryFact = this.factDagResourceFactory.createBinaryFact(ldpResource, descriptionResource as Fact, givenResourceID);
        return binaryFact;
    }

    public async createActivityResource(candidate: ActivityCandidate): Promise<Activity<LdpTrunk>> {
        return this.createResource(candidate, FACTDAG_CLASS.ACTIVITY);
    }

    public async createProcessResource(candidate: ProcessCandidate): Promise<Process<LdpTrunk>> {
        return this.createResource(candidate, FACTDAG_CLASS.PROCESS);
    }

    public async createMessageBrokerResource(candidate: MessageBrokerCandidate): Promise<MessageBroker<LdpTrunk>> {
        return this.createResource(candidate, FACTDAG_CLASS.BROKER);
    }

    public async createAuthorityResource(candidate: AuthorityCandidate): Promise<Authority<LdpTrunk>> {
        return this.createResource(candidate, FACTDAG_CLASS.AUTHORITY);
    }

    //READ Last
    public async getLastResourceRevision(resourceID: IResourceID): Promise<FactDagResource<LdpTrunk>> {
        const ldpResource = await this.ldpClient.getResource(resourceID.resourceURI);
        return this.factDagResourceFactory.createFactDagResource(ldpResource, resourceID);
    }

    public async getLastActivityRevision(resourceID: IResourceID): Promise<Activity<LdpTrunk>> {
        const date = new Date(Date.now());
        const memento = await this.mementoHandler.getResourceRevisionByDateTimeNegotiation(resourceID.resourceURI, date);
        return this.factDagResourceFactory.createActivity(memento, resourceID);
    }

    public async getLastAuthorityRevision(resourceID: IResourceID): Promise<Authority<LdpTrunk>> {
        const date = new Date(Date.now());
        const memento = await this.mementoHandler.getResourceRevisionByDateTimeNegotiation(resourceID.resourceURI, date);
        return this.factDagResourceFactory.createAuthority(memento, resourceID);
    }

    public async getLastProcessRevision(resourceID: IResourceID): Promise<Process<LdpTrunk>> {
        const date = new Date(Date.now());
        const memento = await this.mementoHandler.getResourceRevisionByDateTimeNegotiation(resourceID.resourceURI, date);
        return this.factDagResourceFactory.createProcess(memento, resourceID);
    }

    public async getLastFactRevision(resourceID: IResourceID): Promise<Fact<LdpTrunk>> {
        const date = new Date(Date.now());
        const memento = await this.mementoHandler.getResourceRevisionByDateTimeNegotiation(resourceID.resourceURI, date);
        return this.factDagResourceFactory.createFact(memento, resourceID);
    }

    public async getLastBinaryFactRevision(resourceID: IResourceID, contentType: string): Promise<BinaryFact<LdpTrunk>> {
        const date = new Date(Date.now());
        return await this.getBinary(resourceID, date, contentType);
    }

    public async getResponsibleMessageBroker(authority: Authority<LdpTrunk>): Promise<MessageBroker<LdpTrunk>> {
        const brokerURI = authority.getMessageBrokerURI();
        const brokerResourceID = this.getIDFactory().parseURIToResourceID(brokerURI);
        const date = new Date(Date.now());
        const memento = await this.mementoHandler.getResourceRevisionByDateTimeNegotiation(brokerResourceID.resourceURI, date);
        return this.factDagResourceFactory.createMessageBroker(memento, brokerResourceID);
    }

    // READ SPECIFIC REVISION
    public async getSpecificResourceRevision(factID: IFactID): Promise<FactDagResource<LdpTrunk>> {
        const memento = await this.mementoHandler.getResourceRevisionByDateTimeNegotiation(factID.resourceURI, new Date(factID.revisionID));
        return this.factDagResourceFactory.createFactDagResource(memento, factID);
    }

    public async getSpecificFact(factID: IFactID): Promise<Fact<LdpTrunk>> {
        const date = new Date(factID.revisionID);
        const memento = await this.mementoHandler.getResourceRevisionByDateTimeNegotiation(factID.resourceURI, date);
        return this.factDagResourceFactory.createFact(memento, factID);
    }

    public async getSpecificBinaryFact(factID: IFactID): Promise<BinaryFact<LdpTrunk>> {
        const date = new Date(factID.revisionID);
        return await this.getBinary(factID, date);
    }

    private async getBinary(resourceID: IResourceID, date: Date, resourceType?: string): Promise<BinaryFact<LdpTrunk>> {
        const memento = await this.mementoHandler.getResourceRevisionByDateTimeNegotiation(resourceID.resourceURI, date, true, resourceType);
        const description = await this.getLastFactRevision(resourceID);
        if (memento instanceof LdpNonRdfSource) {
            return this.factDagResourceFactory.createBinaryFact(memento, description, resourceID);
        } else {
            throw new Error('No Binary received');
        }
    }

    public async getSpecificActivityRevision(factID: IFactID): Promise<Activity<LdpTrunk>> {
        const date = new Date(factID.revisionID);
        const memento = await this.mementoHandler.getResourceRevisionByDateTimeNegotiation(factID.resourceURI, date);
        return this.factDagResourceFactory.createActivity(memento, factID);
    }

    public async getSpecificAuthorityRevision(factID: IFactID): Promise<Authority<LdpTrunk>> {
        const date = new Date(factID.revisionID);
        const memento = await this.mementoHandler.getResourceRevisionByDateTimeNegotiation(factID.resourceURI, date);
        return this.factDagResourceFactory.createAuthority(memento, factID);
    }

    public async getSpecificProcessRevison(factID: IFactID): Promise<Process<LdpTrunk>> {
        const date = new Date(factID.revisionID);
        const memento = await this.mementoHandler.getResourceRevisionByDateTimeNegotiation(factID.resourceURI, date);
        return this.factDagResourceFactory.createProcess(memento, factID);
    }

    //READ OTHER
    public async getAllRevisions<ResourceType extends FactDagResource>(resource: ResourceType, type: FACTDAG_CLASS): Promise<ResourceType[]> {
        const timeMap = await this.mementoHandler.getTimemap(resource);
        const urlList = timeMap.getMementoList().map(x => x.url);
        const mementos = await this.mementoHandler.getMementos(urlList);
        const resourceList: ResourceType[] = [];
        mementos.forEach(m => {
            resourceList.push(this.factDagResourceFactory.createFactDagResourceFromType(m, resource.factID, type));
        });
        return resourceList;
    }

    public async getRevisionList(resource: FactDagResource<LdpTrunk>): Promise<IRevision<URL>[]> {
        const timeMap = await this.mementoHandler.getTimemap(resource);
        const mementoList = timeMap.getMementoList();
        return mementoList.map((mementoLocation: MementoLocation) => {
            return {
                factID: this.idFactory.createFactID(resource.factID.authorityID, resource.factID.resourceID, mementoLocation.date.toUTCString()),
                location: mementoLocation.url,
            };
        });
    }

    async getRevisionListByID(resourceID: IResourceID): Promise<IRevision<URL>[]> {
        const ldpResource = await this.getLastResourceRevision(resourceID);
        return this.getRevisionList(ldpResource);
    }

    // UPDATE
    public async createActivityRevision(candidate: ActivityCandidate<LdpTrunk>): Promise<Activity<LdpTrunk>> {
        return this.createResourceRevision(candidate, FACTDAG_CLASS.ACTIVITY);
    }

    public async createAuthorityRevision(candidate: AuthorityCandidate<LdpTrunk>): Promise<Authority<LdpTrunk>> {
        return this.createResourceRevision(candidate, FACTDAG_CLASS.AUTHORITY);
    }

    public async createFactRevision(candidate: FactCandidate<LdpTrunk>): Promise<Fact<LdpTrunk>> {
        return this.createResourceRevision(candidate, FACTDAG_CLASS.FACT);
    }

    public async createBinaryFactRevision(candidate: BinaryFactCandidate<LdpTrunk>): Promise<BinaryFact<LdpTrunk>> {
        let etag = "";
        if (candidate.trunk) {
            etag = candidate.trunk.strongEtag;
        }
        const ldpResourceCandidate = new LdpNonRdfSource(candidate.binaryContent, candidate.resourceID.resourceName, candidate.resourceID.resourceURI, etag);
        const ldpResource = await this.ldpClient.updateLdpNonRDFSource(ldpResourceCandidate, candidate.binaryContent, candidate.contentType);
        const descriptionResource = await this.createResourceRevision(candidate, FACTDAG_CLASS.FACT);
        return this.factDagResourceFactory.createBinaryFact(ldpResource, descriptionResource as Fact, candidate.resourceID);
    }

    public async createProcessRevision(candidate: ProcessCandidate<LdpTrunk>): Promise<Process<LdpTrunk>> {
        return this.createResourceRevision(candidate, FACTDAG_CLASS.PROCESS);
    }

    public async createBrokerRevision(candidate: MessageBrokerCandidate<LdpTrunk>): Promise<MessageBroker<LdpTrunk>> {
        return this.createResourceRevision(candidate, FACTDAG_CLASS.BROKER);
    }

    // DELETE
    public async deleteResource(resourceID: IResourceID): Promise<void> {
         await this.ldpClient.deleteLDPResource(resourceID.resourceURI);
    }

    // OTHER METHODS
    public async getDirectory(resourceID: IResourceID): Promise<Directory<LdpTrunk>> {
        const ldpResource = await this.ldpClient.getContainer(resourceID.resourceURI);
        return this.factDagResourceFactory.createDirectory(ldpResource, resourceID);
    }

    public async isResourceExistent(resourceID: IResourceID): Promise<boolean> {
        return this.ldpClient.isResourceExistent(resourceID.resourceURI);
    }

    public async isResourceBinary(resourceID: IResourceID): Promise<boolean> {
        return await this.ldpClient.isResourceBinary(resourceID.resourceURI);
    }

    public getIDFactory(): IDFactory {
        return this.idFactory;
    }

    public getFactDagResourceFactory(): LDPFactDagResourceFactory {
        return this.factDagResourceFactory;
    }

    // IMPLEMENTATION
    /*
     * This method should only directly create a resource from a candidate, but to create resources that respond to
     * Memento negotiation, Trellis requires at least one revision.
     * Therefore, every creation event instantiates an empty resource first and revisions it afterwards, as a workaround.
     */
    private async createResource<CandidateType extends ResourceCandidate<EmptyTrunk>, ResourceType extends FactDagResource<LdpTrunk>>
    (candidate: CandidateType, factdagClass: FACTDAG_CLASS): Promise<ResourceType> {
        const emptyResource = await this.createEmptyResource(candidate);
        const resourceID = this.idFactory.parseURIToResourceID(emptyResource.resourceURI.toString()); //TODO For creation events, the created factID has to depend on the response, not on the preconstructed FactID}
        candidate.trunk = LDPFactDagResourceFactory.generateTrunk(emptyResource);
        return await this.createResourceRevision(candidate, factdagClass);
    }

    /**
     * This method allows to create empty resources and then revision them to create the first revision.
     * This is necessary to activate the Memento-Behavior of Trellis.
     * @param candidate
     */
    private async createEmptyResource<CandidateType extends ResourceCandidate<EmptyTrunk>, ResourceType extends FactDagResource<LdpTrunk>>
    (candidate: CandidateType): Promise<LdpResource> {
        const location = candidate.resourceID.directory;
        return await this.ldpClient.createLDPRDFSource(location, "", candidate.resourceID.resourceName);
    }

    private async createResourceRevision<CandidateType extends ResourceCandidate<LdpTrunk|EmptyTrunk>, ResourceType extends FactDagResource<LdpTrunk>>
    (candidate: CandidateType, factdagClass: FACTDAG_CLASS): Promise<ResourceType> {
        let etag = "";
        if (candidate.trunk && candidate.trunk instanceof LdpTrunk) {
            etag = candidate.trunk.strongEtag;
        }
        const ldpResourceCandidate = new LdpResource(candidate.serialize(), candidate.resourceID.resourceName, candidate.resourceID.resourceURI, etag);
        const ldpResource = await this.ldpClient.updateLdpRDFSource(ldpResourceCandidate);
        if (ldpResource.headers['memento-datetime']) {
            return this.factDagResourceFactory.createFactDagResourceFromType<ResourceType>(ldpResource, candidate.resourceID, factdagClass);
        } else {
            // Workaround to retrieve a RevisionID, if it is not returned directly during PUT, Caution, this may be unreliable.
            const date = new Date(Date.now());
            const memento = await this.mementoHandler.getResourceRevisionByDateTimeNegotiation(candidate.resourceID.resourceURI, date);
            return this.factDagResourceFactory.createFactDagResourceFromType<ResourceType>(memento, candidate.resourceID, factdagClass, true);
        }
    }
}