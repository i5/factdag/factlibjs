import {IResourceID} from "../../../../datamodels/interfaces/IResourceID";

export class LdpResourceID implements IResourceID {
    private _authorityID: string;
    private _resourceID: string;

    constructor(authorityID: string, resourceID: string) {
        this._authorityID = authorityID;
        this._resourceID = resourceID;
    }

    get authorityID(): string {
        return this._authorityID;
    }

    get resourceID(): string {
        return this._resourceID;
    }

    get resourceURI(): URL {
        const url = new URL(this.authorityID);
        url.pathname = this._resourceID;
        return url;
    }

    get factURI(): string {
        return "fact::" + this.resourceURI.toString();
    }

    get directory(): URL {
        const path = this.resourceID.substring(0, this.resourceID.lastIndexOf('/'));
        const url = new URL(this.authorityID);
        url.pathname = path;
        return url;
    }

    get resourceName(): string {
        return this.resourceID.substring(this.resourceID.lastIndexOf('/')+1, this.resourceID.length);
    }

    toString = (): string => {
        return this.resourceURI.toString();
    }

}
